#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>

int main(int argc, char** argv)
{
    std::ofstream myfile;
    myfile.open ("foo.bar");
    auto now = std::chrono::system_clock::now();
    std::time_t now_time = std::chrono::system_clock::to_time_t(now);
    myfile << std::ctime(&now_time);
    myfile.close();
    return 0;
}
